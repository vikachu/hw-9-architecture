# Wake equipment service  

## Aim
Manage catalog with equipment items available. Possibility to rent or buy item from the catalog.

**Solution** - NodeJS api.

## Connection
HTTP connection throw the API given.

## Data store
Relational DB. For example - PostgreSQL.