# Season tickets service  

## Aim
Responsible for managing tickets types, prices and sales. season ticket gives the ability to book time slot for track. Could be used any time.

**Solution** - NodeJS api.

## Connection
HTTP connection throw the API given.

## Data store
NoSQL DB due to not consistent structure and frequent changes. For example - MongoDB.