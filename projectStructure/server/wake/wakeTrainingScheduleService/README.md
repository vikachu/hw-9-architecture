# Wake training schedule service  

## Aim
Reserve time slot (time, track) and rent equipment if needed.

**Solution** - NodeJA api, Google calendar API.

## Connection
HTTP connection throw the API given.

## Data store
Relational DB. For example - PostgreSQL.