# Training photo and video recording service  

## Aim
Preprocess and save in the store photos and videos from the training. Get link to the resource.

**Solution** - NodeJA api, photo and video preprocessing tools.

## Connection
HTTP connection throw the API given.

## Data store
AWS S3 bucket storage.