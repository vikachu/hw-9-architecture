# Payment service  

## Aim
Pay the invoice. There are two types of invoices payments:  
1. Pay + invoice (replenish the balance with points by cash)  
2. Pay - invoice (pay for the service)  

In first case 3rd party payment systems are used (Portmone, Apple Pay etc.), when invoice is paid - new points 1:1 added to the balance.

**Solution** - NodeJS api, Abstract Factory pattern for the invoice payment type.

## Connection
HTTP connection throw the API given.

## Data store
RabbitMQ.