# Create invoice service  

## Aim
Create an invoice for payment. There are two types of invoices:  
1. Replenish the balance (with cash for points)  
2. Pay for bar/training/rent (with points)  

All operations with the balance should be done through the invoice creating (both replenish the balance and pay for the service). In case of "moneyback" we should have a way to decrease the number of points respectively.  

**Solution** - NodeJS api, Abstract Factory pattern for the invoice type.

## Connection
HTTP connection throw the API given.

## Data store
RabbitMQ.. Invoices are saved in Client DB.