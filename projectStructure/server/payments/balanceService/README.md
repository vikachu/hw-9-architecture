# Balance service  

## Aim
Service has direct acces to the client balance. Possible actions:  
1. Add points  
2. Delete points  
3. Check enough points  
4. Check debt no more than 100 points  

**Solution** - NodeJS api.

## Connection
HTTP connection throw the API given.

## Data store
RabbitMQ.