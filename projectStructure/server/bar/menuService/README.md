# Menu service  

## Aim
Consists of three submodules: generate menu (from products available), generate PDF documents and link for it, generate QR code image. **Solution** - Node JS api server, PDF converter lib, QR code generator.

## Connection
HTTP connection throw the API given.

## Data store
Store for PDF menu document (with access by link) adn QR code images. For example - AWS S3 bucket.