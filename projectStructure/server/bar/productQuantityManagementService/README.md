# Product quantity management service  

## Aim
Manage catalog with products available and their quantity (changes due to orders made). **Solution** - Node JS api server.

## Connection
HTTP connection throw the API given.

## Data store
Relational DB for storing mostly permanent types of products and their quantity. For example - PostgreSQL.