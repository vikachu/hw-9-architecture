# Client side

React for web version (with Saga for async requests and Redux for state management) and ReactNative for mobile application. 
The structure mainly reflects backend domain blocks.