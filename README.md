# System architecture schema

Click [here](https://drive.google.com/file/d/1U5W4wxP_o9hU5u9oWwJibUMcY34k86zn/view?usp=sharing) to open the diagram in draw.io.

![architecture schema](./assets/Architecture.png)

# Architecture explanation
The system design type used here is **microservice architecture**, where services are grouped by domain. This approach was used because of the next reasons:  
- the system requires logically separate modules, which are expected to be isolated (like bar system and wakeboard training)  
- the system is expected to be in the development phase for a long time, so new features would be added frequently  
- due to flexible requirements, there could be plenty of changes wanted during the development phase (for ex. - payment system change)  

That's why microservices where chosen.

## Domain groups and services
1. **User domain**  
The starting point of the application. Contains `Auth service` and `User roles management service`, with are responsible for user login, access rights, and  all the information connected with user entity. 
  

2. **Payment domain**  
All services are related to user invoices and balance change in some way. Here are `Create invoice service`, `Payment service`(with 3rd party calls to the payment system), `Balance service`, and `Promo code` service.  

3. **Bar domain**  
Services, that are logically connected to Bar entity: `Product quantity management system` - a main catalog of available products, `Menu service` with the ability to create a menu, generate link and QR code for it and `Orders service`, which creates invoices on ordered products by the user.  

4. **Wake domain**  
Wake part of the system. Contains `Wake equipment service` for buy or rent wake or other equipment needed, `Wake training schedule service` - management system for reserving time slots for different wake tracks, `Season ticket service` - manages season tickets types, price and sales, and `Training photo and video  recording service` - for processing and saving photo and video data.

5. **Marketing domain**  
`Email newsletter service` for emailing users about events, or sales, etc. `Notification service` - sends notifications about upcoming events, in-app notifications.

6. **Blog domain**  
`CMS system service` - a separate module for blog posts.

7. **Gamification domain**  
`Achievements service` - additional service for integrating achievements system.

## Connection protocols  
`Payment service`, `Balance service`, and `Promo code service` - which all influence client balance - are decided to use **RabbitMQ** message broker (due to all actions with balance should be consistent). The other services would use standard APIs and **HTTP** connections.